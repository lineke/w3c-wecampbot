<?php

require 'vendor/autoload.php';

use DogUrlFetcher\FileGetContentsDogUrlFetcher;
use PhpSlackBot\Bot;
use QuoteFetcher\ForismaticApi;
use W3C\Cat;
use W3C\Dog;
use W3C\Erik;
use W3C\FirstAid;
use W3C\Gitlab;
use W3C\Halp;
use W3C\Help;
use W3C\Jeremy;
use W3C\Lineke;
use W3C\Michelle;
use W3C\Packagist;
use W3C\Quote;
use W3C\Ramon;
use W3C\Restart;
use W3C\SelfUpdate;
use W3C\Schedule;
use W3C\WhatTheCommit;
use \W3C\Weather;

$bot = new Bot();
$bot->setToken(getenv('SLACK_TOKEN'));

if (getenv("OPENWEATHERMAP_APIKEY") === false) {
	die('Missing ENV var OPENWEATHERMAP_APIKEY');
}

/**
 * This is where the magic happens
 */
$bot->loadCommand(new Help());
$bot->loadCommand(new Halp());
$bot->loadCommand(new Restart());
$bot->loadCommand(new Schedule());
$bot->loadCommand(new SelfUpdate());
$bot->loadCommand(new WhatTheCommit());
$bot->loadCommand(new Weather(getenv("OPENWEATHERMAP_APIKEY")));
$bot->loadCommand(new Cat());
$bot->loadCommand(new Dog(new FileGetContentsDogUrlFetcher()));
$bot->loadCommand(new Ramon());
$bot->loadCommand(new Jeremy());
$bot->loadCommand(new Quote(new ForismaticApi()));
$bot->loadCommand(new Erik());
$bot->loadCommand(new FirstAid());
$bot->loadCommand(new Packagist());
$bot->loadCommand(new Gitlab());
$bot->loadCommand(new Michelle());
$bot->loadCommand(new Lineke());

$bot->run();
