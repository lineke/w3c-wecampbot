<?php

declare(strict_types=1);

namespace W3C;

use PhpSlackBot\Command\BaseCommand;

final class Packagist extends BaseCommand
{
    protected function configure()
    {
        $this->setName('!packagist');
    }

    protected function execute($message, $context)
    {
        $this->send($this->getCurrentChannel(), null, 'To use our private Packagist installation, go to https://packagist.weca.mp/. Click on the WeCamp organization to get instructions on how to use private Packagist.');
        $this->send($this->getCurrentChannel(), null, 'If you don\'t see the organization, talk to your coach or someone of the crew. We\'ll have to run the synchronization');
    }
}
