<?php

declare(strict_types=1);

namespace W3C;

use PhpSlackBot\Command\BaseCommand;

final class Michelle extends BaseCommand
{
    protected function configure()
    {
        $this->setName('!michelle');
    }

    protected function execute($message, $context)
    {
        $this->send($this->getCurrentChannel(), null, 'https://youtu.be/9M_FlkAiTSo?t=21');
    }
}
