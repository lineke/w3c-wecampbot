<?php

namespace W3C;

use \PhpSlackBot\Command\BaseCommand;

class Help extends BaseCommand
{
    protected function configure()
    {
        $this->setName('!help');
    }

    protected function execute($message, $context)
    {
        $commands =  <<< EOC
Known commands:

- !help (You are looking at it)
- !commit (Suggests a commit message for your next commit)
- !packagist (For help with how to use our private Packagist)
- !gitlab (For information on how to access our local Gitlab installation)
- !schedule [day] (Shows the schedule for the day, i.e. !schedule friday)
- !weather (Show the weather forecast)
- !firstaid (Shows the phone numbers for first aid emergencies)
- !cat (Show a cat. Like duh. Seriously, what did you expect. Why are you still reading?)
- !dog (Show a dog. Far inferior to !cat. Do not use. Like ever)
- !ramon (If you really need him)
- !jeremy (He really loves having Copacabana stuck in his head, tease with ease)
- !quote (Show an inspirational quote)
- !erik (If you feel like going to the Efteling)
- !michelle (She really loves having Baby Shark stuck in everybody else's head)
- !lineke (Because we can)

Add more? Go ahead! https://gitlab.com/WeCamp/w3c-wecampbot
EOC;
        $this->send($this->getCurrentChannel(), null, $commands);
    }
}
